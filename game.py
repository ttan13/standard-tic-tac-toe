from difflib import IS_LINE_JUNK
from re import M


def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def print_and_exit(board, index):
    print_board(board)
    print(board[index], "has won")
    exit()

def is_line_winner(board, start, a, m):
    n = (start - 1) * m
    return board[n] == board[n + a] and board[n + a] == board [n + 2 * a]


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    for i in range(1, 4):
        if is_line_winner(board, i, 1, 3):
            print_and_exit(board, (i-1) * 3)
        if is_line_winner(board, i-1):

if board[0] == board[4] and board[4] == board[8]:
    print_and_exit(board, 0)
elif board[2] == board [4] and board[4] == board[6]:
    print_and_exit(board, 0)

if current_player == "X":
        current_player = "O"
else:
        current_player = "X"
 
print("It's a tie!")


 